//
//  SchoolDetailsViewModelTests.swift
//  20230314-ShyamSundar-NYCSchoolsTests
//
//  Created by ShyamSundar on 3/15/23.
//

import XCTest

final class SchoolDetailsViewModelTests: XCTestCase {
let schoolDetailsViewModel = SchoolDetailsViewModel()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testInitialsNumberOfRowsCount() {
        XCTAssertEqual(schoolDetailsViewModel.rows.count, 1)
    }
    func testRowsTitle() {
        
        XCTAssertEqual(schoolDetailsViewModel.getScoreDetailsData(forRow: .numberOfSatTestTakers), "Number Of Sat Test Takers: NA")
        XCTAssertEqual(schoolDetailsViewModel.getScoreDetailsData(forRow: .criticalReadingScore), "Sat Critical Reading Avg Score: NA" )
        XCTAssertEqual(schoolDetailsViewModel.getScoreDetailsData(forRow: .mathScore),"Sat Math Avg Score: NA" )
        XCTAssertEqual(schoolDetailsViewModel.getScoreDetailsData(forRow: .writingScore), "Sat Writing Avg Score: NA")
        
    }
}
