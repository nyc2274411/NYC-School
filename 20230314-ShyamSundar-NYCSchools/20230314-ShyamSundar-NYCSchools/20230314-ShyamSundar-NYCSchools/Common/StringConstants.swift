//
//  StringConstants.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 15/03/23.
//

import Foundation

struct StringConstants {
    
    struct SchoolList {
        static let title = "Schools".localized()
    }
    
    struct SchoolDetails {
        static let title = "School Details".localized()
        static let numberOfSatTestTakers = "Number Of Sat Test Takers: ".localized()
        static let satCriticalReadingAvgScore = "Sat Critical Reading Avg Score: ".localized()
        static let satMathAvgScore = "Sat Math Avg Score: ".localized()
        static let satWritingAvgScore = "Sat Writing Avg Score: ".localized()
        static let notAvailable = "NA".localized()
        static let emptyString = "".localized()
    }
}
