//
//  SchoolNameTableViewCell.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import UIKit

class SchoolNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setSchoolName(name:String) {
        self.schoolName.text = name
    }
}
