//
//  SchoolsListViewController.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import UIKit

class SchoolsListViewController: UIViewController {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    let viewModel = SchoolsListViewModel()
    
    private struct Attributes {
        static let schoolNameCellIdentifier = "SchoolNameTableViewCell"
        static let schoolDetailsViewControllerIdentifier = "SchoolDetailsViewController"
        static let mainStoryBoardIdentifier = "Main"
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = StringConstants.SchoolList.title
        activityIndicatorView.startAnimating()
        viewModel.getSchoolsList(completion: { isSuccess in
            self.activityIndicatorView.stopAnimating()
            if isSuccess {
                self.tableView.reloadData()
            }
        })
    }
}

extension SchoolsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schoolsData?.schools.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Attributes.schoolNameCellIdentifier, for: indexPath) as? SchoolNameTableViewCell {
            if let name = viewModel.schoolsData?.schools[indexPath.row].schoolName {
                cell.setSchoolName(name: name)
            }
            return cell
        }
        return UITableViewCell()
    }

}

extension SchoolsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let vc = UIStoryboard.init(name: Attributes.mainStoryBoardIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: Attributes.schoolDetailsViewControllerIdentifier) as? SchoolDetailsViewController {
            vc.viewModel.selectedSchool = viewModel.schoolsData?.schools[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
