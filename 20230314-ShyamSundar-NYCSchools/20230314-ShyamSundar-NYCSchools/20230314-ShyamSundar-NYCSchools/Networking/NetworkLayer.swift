//
//  NetworkLayer.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import Foundation
import Alamofire

class NetworkLayer {
    
    func getSchoolsList(endPoint: String, completion: @escaping (SchoolsList?, Bool) -> ()) {
        
        AF.request(endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            
            guard let data = responseData.data else { return }
            
            do {
                let schoolsList = try JSONDecoder().decode([School].self, from: data)
                let schools = SchoolsList(schools: schoolsList)
                completion(schools, responseData.response?.statusCode == 200)
            } catch {
                completion(nil, false)
            }
            
        }
    }
    
    func getSelectedSchoolsDetails(dbn: String, endPoint: String, completion: @escaping (SchoolDetailsModel?, Bool) -> ()) {
        
        AF.request(endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            
            guard let data = responseData.data else { return }
            
            do {
                let schoolsDetails = try JSONDecoder().decode([SchoolDetailsModel].self, from: data)
                completion(schoolsDetails.first, responseData.response?.statusCode == 200)
            } catch {
                completion(nil, false)
            }
            
        }
    }
}
